\chapter{Detection of the spatial mesh}
\label{ch:det}
The goal of this chapter is to describe the procedure to detect the spatial mesh, estimate its state (pose \acrshort{wrt} some inertial frame) and feedback it to the architecture computer. These measurements are needed in order to make sure that a) no collisions occur during the welding process of the mesh mold and b) the computer model of the mesh mold is held up to date to ensure an accurate fabrication process. \\
The challenge in solving this task is that the structure of the mesh mold is a repetitive composition of  small steel wires (2.5mm in our experiments, see \ref{fig:sub2}) which makes detection with off-the-shelf solutions hard. Two types of sensors were considered in order to solve the problem: cameras and lasers. Even though high-end lasers from e.g. Micro-Epsilon\footnotemark[8] are very accurate, they rely on signal reflection at the target object. Since the wires are quite small however, they might not be detectable with a laser. Moreover, those lasers are quite bulky and very expensive. The second option is the use of cameras. Experiments were conducted generating a depth map using and off-the-shelf stereo vision approach\footnotemark[9]. Problems occur when trying to match corresponding points in the two stereo images because of the periodic structure of the spatial mesh. Similar problems occur when trying to use a state-of-the-art line matching algorithm as presented in\footnotemark[10]. \\
We therefore propose a tailor-made, problem specific stereo vision approach. An advantage of the \acrshort{if} system environment is that we do have a computer model of the mesh mold. We can use this information as an approximated guess of the wires that we are trying to measure. Consider figure 

\footnotetext[8]{\url{http://www.micro-epsilon.co.uk/laser-scanner/scanCONTROL/model-overview/index.html}}
\footnotetext[9]{\url{http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_calib3d/py_depthmap/py_depthmap.html}}
\footnotetext[10]{\url{http://docs.opencv.org/3.0-beta/modules/line_descriptor/doc/tutorial.html}}
\section{Definition of the algorithm}
\ref{fig:wiredet2} which illustrates our used mesh detection algorithm on a system level.
\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth]{/home/manuel/master_thesis/t_student_report/images/wiredet2.pdf}
\caption{The high-level steps of the mesh detection algorithm}
\label{fig:wiredet2}
\end{figure} 
A node is the product of welding a discrete and a continuous wire. The cameras are pointed towards the center of the tool (end-effector) where the welding of those nodes takes place. The whole idea of the algorithm is to find the 3d information of the node that is right beneath the node that is about to be welded. When we correctly detect such a node, we can calculate the 3D data of each wire (discrete and continuous), adjust the end-effector accordingly, weld collision free and update the computer model if needed.\\ A wire is assumed to have the shape of a line. Thus, our feature detector is based on detecting lines. We are also trying to find at least two lines to ensure three linearly independent points in 3D which form a pose. We feedback this information to the architecture computer to ensure an accurate welding process.
\subsection{Stereo rectification}

Stereo rectification consists of two steps: a) rectify each image to get rid of distortion and b) align the two images such that they are lying on the same plane. This makes all the epipolar lines parallel and simplifies the stereo correspondence problem, see \ref{fig:stereorect}. We use the well-know opencv function \textit{stereoRectify()}\footnotemark[10] which use is demonstrated in the open-cv sample code \textit{stereo\_calib.cpp}\footnotemark[11]. More information regarding camera calibration can be found in \cite{opencv}\cite{camera}. \acrshort{ros} also provides an off-the-shelf camera stereo calibration\footnotemark[12], however it was not used since it does not provide all the data needed for our problem specific calculation of the 3d data, see \ref{subsec:corr}. As to be depicted in figure \ref{fig:wiredet2}, the cameras are tilted towards the center of the tool which is where the point of interest is lying: the mesh. Note that this angle has a certain limit because aligning the two images will not be possible anymore when it exceeds a certain limit. According to our hardware experiments, the current setup (approx. 30 degrees from camera to camera) is the maximum tilt angle which still gives reasonable stereo calibration results.
\footnotetext[10]{\url{http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html}}
\footnotetext[11]{\url{https://github.com/opencv/opencv/blob/master/samples/cpp/stereo_calib.cpp}}
\footnotetext[12]{\url{http://wiki.ros.org/camera_calibration/Tutorials/StereoCalibration}}
\footnotetext[13]{\url{http://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html}}
\label{subsec:stereorect}
\begin{figure}[!htb]
\centering
\includegraphics[width=1.0\linewidth]{images/stereorectification.pdf}
\caption{Stereo rectification explained while \acrshort{if} is building the spatial mesh.}
\label{fig:stereorect}
\end{figure} 

\subsection{Feature detection and 2D line filtering}


\label{subsec:feat}
\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/featuredet.pdf}
\caption{Feature (lines) detection and the 2D filtering illustrated step by step}
\label{fig:featuredet}
\end{figure} 
After stereo rectifying the two images the following steps are applied (consider figure \ref{fig:featuredet} which illustrates all these steps):
\begin{enumerate}
	\item \textbf{Image blurring:} In order to get rid of noise, we blur the image using a normalized box filter\footnotemark[13]. It would also be possible to use a Gaussian kernel to blur the image\footnotemark[13].\footnotetext[14]{\url{http://docs.opencv.org/2.4/doc/tutorials/imgproc/erosion_dilatation/erosion_dilatation.html}}
	\item \textbf{Image dilation:} Consider the left rectified image of figure \ref{fig:stereorect}. There are wires in the background that we are not interested in. Clearly, the wires in the background are farer away than the wires in the foreground which makes them appear smaller. This is why we use dilation, which makes objects with the same shape smaller\footnotemark[14]. In our case, this will make the wires in the background disappear whereas the wires in the foreground will just become smaller and still be detectable as lines.

	\item \textbf{Canny Edge detector:} We use the well known Canny-Edge detector \cite{canny} implemented in \acrshort{cv}\footnotemark[15] to find edges in the image. It is a preliminary for the the next step.
	
	\footnotetext[15]{\url{http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/canny_detector/canny_detector.html}}
	\footnotetext[16]{\url{http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_lines/hough_lines.html}}
	
	\item \textbf{Hough line transformation:} We use the probabilistic Hough transform \cite{hough} implemented in \acrshort{cv}\footnotemark[16] to detect the two side of the wire as lines.
	\item \textbf{2d line filtering:} This step gets rid of lines that most probably do not belong to the wires we are trying to find. We define two filters: a) the wire must be under the welding clamps b) the wire must lie in a certain angle range. The filter parameters are chosen in such a way that it is highly unlikely for the filter to be discarding the wire we are looking for. The filter parameters are adjusted for the continuous and the discrete wire separately.
\end{enumerate}


\subsection{Calculation of a wire in 3D using a corresponding line pair}
\label{subsec:corr}

For the upcoming sub section we assume that we have a corresponding line pair $l_{l}$, $l_{r}$ in the left  and the right image. The procedure for choosing the correct line pair is described in \ref{subsec:3dmatching}. 
The calculation of the 3d data is done as follows (consider figure \ref{fig:3dcalc} which illustrates all these steps):

\begin{enumerate}
	\item Take an arbitrary point $p_{l}$ on the left line $l_{l}$. 
	\item Generate the epipolar line $e$ of this point.
	\item Calculate the intersection point $p_{r}$ between the epipolar line $e$ and the right line $l_{r}$.
	\item The two corresponding points $p_{l}$ and $p_{r}$ can be used together with the outputs of the stereo rectification described in \ref{subsec:stereorect}. We need the matrix $Q$ which maps the 2d coordinates of the correspondence points to real world 3d data. Note that these 3d data are expressed in the \textit{left} camera frame of the image \textit{after} stereo rectifying it (see \textit{stereoRectify()} \acrshort{api}\footnotemark[15]). Remember that stereo rectification virtually rotates the two cameras such that the image planes lie on the same plane. However, this is not how the cameras are physically mounted. That why we virtually need to rotate it back to the original mount position. We call that rotation $T_{C_{o}C_{r}}$. 3D data of a point on the line expressed in the original frame of the left camera is calculated as follows:
	\begin{equation}
	\label{eq:3dcalc}
	t_{C_{o}l} =T_{C_{o}C_{r}} * Q * \begin{bmatrix} p_{l}.y \\ p_{l}.x - p_{r}.x \end{bmatrix} 
	\end{equation}
\end{enumerate}
as to be depicted in \ref{fig:3dcalc}, $p_{l}.y = p_{r}.y$ .
We do the above procedure for another point on the line to fully characterize it. It does not depend whether we look for a discrete or and continuous wire. The procedure relies on correctly matching lines in the two images, see \ref{subsec:3dmatching}.

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/3dcalc.pdf}
\caption{Point correspondence search using a line pair}
\label{fig:3dcalc}
\end{figure} 

\subsection{Finding the best-fit corresponding line pair in 3D}
\label{subsec:3dmatching}
The last step of the algorithm is finding two correct correspondence line pairs: one for the continuous wire and one for the discrete wire. The reason why we have to find two wires is, as mentioned, that we need three linearly independent points in 3d in order to define a mesh node pose \acrshort{wrt} the left camera frame. One wire would only give us two points which is not sufficient. Remember that we have a computer model of the mesh mold which gives us an expected 3d position of the two wires we are looking for. Thus, the idea is to try out all combinations of lines found with the method described in \ref{subsec:feat}, project them to 3d using \ref{subsec:corr} and try to find the two pairs that best matches our expected wire from the computer model. \\
To achieve that goal, we use a cost function that finds the best 3D line match. We describe all the metrics of that cost function using the following notation: \\
\begin{table}[!ht]
\begin{tabular}{|>{\centering\arraybackslash}m{1cm}|>{\centering\arraybackslash}m{12.1cm}|}
	\hline $l_{lc}$ & A continuous line in the left image.  \\ 
	\hline $l_{rc}$ & A continuous line in the right image. \\ 
	\hline $\tilde{W}_{c}$ & The (measured) start and the end point of a continuous wire in 3d that is calculated using a pair $l_{lc}$, $l_{rc}$ (see \ref{subsec:corr}).  \\
	\hline $\tilde{W}_{c_{0..n}}$ & All possible combinations of the line pairs $l_{lc}$ and $l_{rc}$ that results in a wire $\tilde{W}_{c}$. Or in other words $\tilde{W}_{c_{0..n}} = l_{lc} \times l_{rc}$   \\
	\hline $\hat{W_{c}}$ & The (expected) start and end point of a continuous wire from the computer model.\\ 	
	\hline $\hat{p_{c}}$ & The middle point of $\hat{W_{c}}$\\ 	
	\hline 
\end{tabular} 
\label{tab:notations2}
\caption[Notation used for the definition of the cost function]{Notation used for the definition of the cost function}
\end{table}

The same notation is used for a discrete line/wire abbreviated with the letter $d$. Note that all 3d data are described in the left camera frame. \\
The metrics described below are adapted to the characteristics of the probabilistic Hough Transformation \cite{hough}.

\subsubsection{Closest distance from expected wire to measured wire}
$\tilde{W}$ is a 3d line segment with a start and an end point. \\ The function  $pointToWireDist(\tilde{W},\hat{p})$ calculates the closest distance between the middle point $\hat{p}$ and the wire $\tilde{W}$. If the closest distance from $\hat{p}$ to $\tilde{W}$ lies within the line segment, this distance is returned. If it does not lie within the line segment the closest distance to one of the wires endpoint is returned.
\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/pointtowiredist.pdf}
\caption{For $p1$ the closest distance lies within the line segment, for $p2$ the closest distance is at the end point of the line segment. }
\label{fig:pointtowire}
\end{figure} 
This metric is used to calculate how close the measured wire is to the expected wire. The math behind $pointToWireDist(\tilde{W},\hat{p})$ can be found in\footnotemark[17].

\footnotetext[17]{\url{http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html}}

\subsubsection{Angle between expected and measured wire}
The metric $angle(\tilde{W},\hat{W})$ is the angle between the expected and the measured wire. It is defined by:
\begin{equation}
\cos{\phi} = \frac{\tilde{v} \cdot \hat{v}}{\norm{\tilde{v}}\norm{\hat{v}}}
\end{equation} 
whereas $v$ is the vector from the start to the end point of $W$.
\subsubsection{Distance from continuous wire to discrete wire}
As a last metric, we somehow need to measure how well the continuous wire is connected to the discrete wire. If figure \ref{fig:wiredet2} is considered, the two wires are connected with each other during the welding process which makes their closest distance to each other very small. To calculate the distance from wire to wire we define the function $wireToWireDist(\tilde{W_c},\tilde{W_d})$ as follows: we take one endpoint of the continuous wire, if its closest distance is \textit{not} within the line segment of the discrete wire we do the same as in $pointToWireDist()$. However, if the closest distance from that point lies \textit{within} the line segment we treat the two wires as infinite lines and calculate their closest distance according to\footnotemark[18]. This is done because Hough line transformation can also find line segments that are part of the correct wires but do not reach the welding point.
\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/wiretowire.pdf}
\caption{For the calculation of the distances continuous wire 1 is treated as a infinite line whereas  wire 2 is treated as a line segment. }
\label{fig:wiretowire}
\end{figure}\footnotetext[18]{\url{https://en.wikipedia.org/wiki/Skew_lines}}
\subsubsection{Definition of the cost function}
The cost function is defined as:
\begin{equation}
\begin{aligned}
W &= pointToWireDist(\tilde{W}_{c},\hat{p}_c) + A \cdot angle(\tilde{W}_{c},\hat{W_c}) \\ &\quad+  B \cdot  pointToWireDist(\tilde{W}_{d},\hat{p}_d) + C \cdot angle(\tilde{W}_{d},\hat{W}_{d}) \\ &\quad+  D \cdot wireToWireDist(\tilde{W_c},\tilde{W_d})
\end{aligned}
\end{equation}
$A-D$ are weighting for the cost metrics. We use the cost function $W$ for all combinations of $\tilde{W}_{c_{0..n}} \times \tilde{W}_{d_{0..m}} $. The minimum values of $W$ gives us the best 3d match of the continuous and discrete wire.
\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/costfunction.pdf}
\caption{Result of using the cost function to find the best match for the continuous wire $\tilde{W}_{c}$ and discrete wire $\tilde{W}_{d}$}
\label{fig:costfunction}
\end{figure}  



\subsection{Algorithm discussion}
\label{sec:discussion}
\subsubsection{Complexity}
The cost function considers all combinations of $\tilde{W}_{c_{0..n}} \times \tilde{W}_{d_{0..m}} = l_{lc} \times l_{rc} \times l_{ld} \times l_{rd}$. Assume that the line detector found $n$ lines for each line category then the complexity of the algorithm is $\mathcal{O}(n^{4})$. Trying to match more than two wires would increase the robustness of the algorithm but the runtime would explode.
\subsubsection{Worst case}
\label{subsubsec:worstcase}
The worst case occurs when the expected wire is so much off that it is closer to a wrong mesh node than to a correct one, see figure \ref{fig:worstcase}. 
This also gives us the reliability of the matching step. Let the distance between two neighbored discrete wires be $d_c$ and the distance between two discrete wires be $d_d$. The matching fails if the expected continuous \textit{and} the discrete wire is off \textit{more than} $\frac{d_c}{2}$, $\frac{d_d}{2}$  \textit{in the worst case-direction}. As mentioned, the idea of the algorithm is to correct the computer model after every measurement, so the expected wire should always be very similar to the actual (measured) one. The critical point occurs after repositioning of the base, when we use the state estimation to determine the new pose of the robot and calculate the reattachment pose of the tool. This is why the state estimation of the robot also has to fulfill the worst case criterion mentioned above. Roughly speaking, assume for example that $d_c=2cm$. Then, the robot pose estimation would require an accuracy of $1cm$ to ensure an autonomous detection of the wires after reattaching the tool. 

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth]{images/worstcase.pdf}
\caption{Illustration of the wire detection worst case }
\label{fig:worstcase}
\end{figure} 

\subsubsection{Centering lines}
Consider figure \ref{fig:featuredet}. Hough line transformation finds the lines on the edge of the wire. Even after dilating the image, the lines are not yet in the middle of the wire. We could invent some heuristics to find the middle of the wires such as averaging line that are very close to each other or similar approaches. However, inventing heuristics always comes down to situational tuning of parameters. Consider step 4 of figure \ref{fig:featuredet}. Even though we use dilation, some wires in the background are still found very close to the actual wire we are looking for. Averaging those lines or invent some other tactics to center them would distort lines already in the very early steps when we are still in 2D. In addition, if step 5 of figure \ref{fig:featuredet} is considered, the lines are very roughly around +/- 0.5mm away from the center of the wire (diameter is 2.5mm).
\subsubsection{Importance of the line detector}
The whole algorithm is based on the characteristics of the line detector we use. We need to find the correct lines in both images, otherwise the mesh detection fails. If we have hindered conditions such as strong sun light it can happen that the contrast is so bad that we can't detect the lines anymore, see figure \ref{fig:sunlight}. Therefore, we propose the use of additional lighting sources such as high-power LED's at the end-effector. Alternatively, the end-effector could be sheltered from sunlight.
\begin{figure}[!ht]
\centering
\includegraphics[width=1\linewidth]{images/l117.png}
\caption{Contrast is insufficient because of bad lighting conditions due to direct sun light radiation. Lines of the correct wires are not found and therefore the matching step fails.}
\label{fig:sunlight}
\end{figure} 

\subsubsection{Failure handling}
When the line matching somehow fails due to bad light conditions, expected wire deviation or other reasons we get an incorrect wire measurement. However, during our hardware tests (see chapter \ref{ch:hw}), we observed that whenever a mesh detection failure occurred it was clearly observable by comparing with measurements from a previous node. Thus, if the pose difference of two consecutive node measurements exceeded a certain limit, it could be classified as a possible measurement failure. We could also analyze the pose difference to the expected wire of two consecutive node measurement. In case of possible measurement failure, the system could stop and request the user to manually match the lines in the two stereo images.

\subsubsection{Other approaches}
One idea that we briefly discussed during the thesis is finding the correct wires using an optimization approach  similar as in chapter \ref{ch:est}. As an initial guess we would take the expected wires from the computer model. The optimization problem would need to be formed in such a way, that the solution of it would converge towards the correct, actual wires. Since the algorithm presented here delivered sufficient and reliable results in the hardware experiments (\ref{ch:hw}) we did not need to come up with an alternative approach. However, it would be very interesting to see how using optimization would perform.
\clearpage
\section{Interface to the architecture computer}
\label{sec:archif}
The interface to the architecture computer (design environment) is very straightforward:
\begin{figure}[!ht]
\centering
\includegraphics[width=1\linewidth]{images/interface.pdf}
\caption{Interface to the architecture computer}
\label{fig:interface}
\end{figure} 


We get $\hat{W}_{c}$ and $\hat{W}_{d}$ from the architecture computer (design environment) over TCP/IP, which is stored by the computational model of the mesh mold. Since the lines found by the Hough transformation can differ in length, we return the midpoint and unit direction vector of the measured wires. Back at the architecture computer, we calculate the pose of the detected mesh node \acrshort{wrt} the left camera frame, namely $\tilde{T}_{c_{l}m}$,  as follows: \\
\begin{figure}[!ht]
\centering
\includegraphics[width=1\linewidth]{images/meshpose.pdf}
\caption{definition of the mesh \acrshort{cf}}
\label{fig:meshpose}
\end{figure} 

$p_1$, $p_2$ and $p_3$ are three linearly independent points in 3d that form the pose of the mesh.


Assume that we already have a calibrated workspace, we can then calculate: 
\begin{equation}
\label{eq:mesh}
\tilde{T}_{wm} = \tilde{T}_{wr}*T_{re}*\tilde{T}_{ec_{l}}*\tilde{T}_{c_{l}m}
\end{equation}
Refer to figure \ref{fig:overview} and table \ref{tab:notations} for explanation of the notations used in the equation above. $\tilde{T}_{ec_{l}}$ is estimated similarly to the calibration procedure explained in \ref{subsec:basecalib} with the only difference that tag and camera are switched. We call that calibration step \textit{mesh camera calibration}.
Having $\tilde{T}_{wm}$, we can achieve the goal of the spatial mesh detection work package which is: a) adapt $T_{RE}$ (done by the industrial arm) accordingly to ensure a collision free building process (we call this the \textit{correction step}: the industrial arm corrects the pose difference between $\tilde{T}_{wm}$ and $\hat{T}_{wm}$) b) update the design environment (computer model) of the architecture computer.