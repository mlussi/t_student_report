\chapter{Introduction}
\section{Problem statement}
\label{sec:problem}
The \acrlong{if} (\acrshort{if}) from ETH Zurich is a mobile fabrication robot intended to complete autonomous building tasks directly on a construction site. It was developed by the \acrlong{adrl} (\acrshort{adrl})\footnotemark[1] and the group of Gramazio Kohler Research\footnotemark[2] \cite{if2}. It consists of an industrial robotics arm mounted on a base driven by hydraulic crawler tracks. \acrshort{if} can achieve a maximum speed of 5 km/h on a flat terrain and weights a total of 1.3 tons. The whole system is electrically powered and the tracks are hydraulically actuated \cite{if2}. \\
By attaching an appropriate end effector (also called tool) to the  robotics arm, \acrshort{if} can be used for various construction tasks. In earlier physical experiments, the robot was able to autonomously fabricate an undulated dry-stacked brick wall in a laboratory environment set up to mimic a construction site \cite{if1}. In recent experiments, the robot is used to autonomously build the mold of a user-defined, undulated wall. It does that by welding vertical and horizontal steel wires resulting in a spatial mesh. After building the mold, it is manually filled-up with a tailor-made cement to finish the fabrication \cite{meshmold}. The shape of the wall can be designed with an architectural planning software.


\begin{figure}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{images/if1.JPG}
  \caption{base, arm and end effector}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=.62\linewidth]{images/mesh.jpg}
  \caption{end effector and mesh mold (source\footnotemark[3])}
  \label{fig:sub2}
\end{subfigure}
\caption{The \acrshort{if} and the mesh mold}
\label{fig:test}
\end{figure}

\footnotetext[1]{\url{http://www.adrl.ethz.ch}}
\footnotetext[2]{\url{http://www.gramaziokohler.arch.ethz.ch}}
\footnotetext[3]{\url{http://gramaziokohler.arch.ethz.ch/web/forschung/e/0/0/0/316.html}}


\acrshort{if} is already able to autonomously build small parts of the mesh mold. However, there are two main features missing: 
\begin{itemize}
	\item \textbf{Robot base state estimation:} The robot base does not have any autonomous pose estimation. Whenever the base is repositioned, it has to be calibrated using a manual measurement procedure that takes around 30 minutes of human work. Since the arms reachability is limited, repositioning will take place while building. Moreover, depending on the arm position, the pose of the base can change due shifting of the robots center of mass.
	\item \textbf{End effector feedback:} Even though the industrial robotic arm has a very high relative accuracy, the end effector which welds the wires resulting in the spatial mesh does not have any feedback of what it built such as lasers or cameras. Thus, while continuously building small parts (called nodes) of the mesh, accumulative errors can be observed which results in collisions while fabricating the wall. This means that the whole building process has to be supervised by a human who manually corrects the errors appearing while building. Moreover, the wall will not be fabricated like it was planned in the design environment, since there is no method to update the computer model.
\end{itemize}

\section{System overview}
\acrshort{if} system environment can be broken down into the following functional units:
\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/systemoverview.pdf}
\caption{The system components of the \acrshort{if} distinguishing between parts that exists and parts that still have to be developed.}
\label{fig:sysoverview}
\end{figure} 

\clearpage

For the design environment we use the architectural planning tool Grasshopper Rhinoceros\footnotemark[4]. It is responsible for designing the mesh mold as well as planning the high level fabrication tasks. As to be depicted in figure \ref{fig:sysoverview}, this includes the sequencing of the industrial arm and the end-effector. Note, that in the recent stage of the project, the tracks are manually controlled using a joystick. However, the vision for later stages of the project is to also connect it to the design environment to ensure a fully autonomous building process. \\
The system does not yet have sensor units attached to the end-effector or the robots base. This would ensure a feedback to the design environment regarding a) the robots base pose \acrshort{wrt} some inertial frame b) the pose of relevant parts of the mesh ensuring a accurate and collision free fabrication process.

\footnotetext[4]{\url{http://www.grasshopper3d.com}}

\section{Goal}
The missing parts depicted in figure \ref{fig:sysoverview} have to be designed, implemented and tested.
The goal of this thesis is to develop a system that allows the \acrshort{if} to collision-free, robustly and accurately build the mesh mold. The robot should be able to autonomously reposition its base whenever required and reattach the end effector to continue building. Note that it is impossible to drive the \acrshort{if} without the arm being completely pulled in. The system has to work on real hardware and has to have well-defined interfaces in order to interact with the architecture high level planning software. 

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/build.pdf}
\caption{\acrshort{if} building mesh mold step by step. Before every base repositioning the arm has to be pulled in and reattached afterwards to continue building. }
\label{fig:build}
\end{figure} 

\clearpage

\section{Thesis structure}
The thesis is structured as follows:

\begin{itemize}
	\item \textbf{\nameref{ch:est}:} This chapter describes how the base pose of the \acrshort{if} in the world frame is calculated. It also includes the definition of the workspace calibration procedure that has to be done to determine and store the world location of rigid system components (an example would be the location of a camera, a tag etc.). In addition, two different formulations of the optimization problem are introduced and compared.
	\item \textbf{\nameref{ch:det}:} This chapter describes how the mesh is detected, its 3D world coordinates are calculated and feed backed to the architecture computer.
	\item \textbf{\nameref{ch:sys}:} This chapter gives an overview of the used software and hardware components relevant for this thesis and shows how they interact with each other.
	\item \textbf{\nameref{ch:hw}:} This chapter shows the test cases and results of the presented system running on the \acrlong{if}. Moreover, it provides important hints of how to get the best performance out of the system.
\end{itemize}
The thesis is rounded off with a summary and future work.