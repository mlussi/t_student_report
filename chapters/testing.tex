\chapter {Hardware experiments}
\label{ch:hw}
Over the duration of 4-5 weeks the whole software was integrated and tested on the real \acrshort{if} hardware. Unit and system tests were conducted which resulted in many data that can be found in the git repository of this thesis. This section only summaries the most important outcomes of those tests and also shows the tricks applied to get the best results. For the setup used during the hardware experiments refer to figure \ref{fig:overview}. 

\section {Workspace calibration tuning}
\begin{itemize}
	\item Make sure that the tags are printed with a good printer to ensure an accurate size. Make sure they are glued on a stiff, rigid component such as Plexiglas or thick cardboard.
	\item Do not swipe around with the cameras to fast to prevent skewed images that result in distorted tag measurements.
	\item We have video proofs, captured during the experiments, that different light conditions can change tag measurements which results in position difference at the end-effector of roughly 1cm. The exact reasons for it are unknown at this stage of the project, however we verified that the number of visible tags were the same with different light conditions. Therefore, make sure to calibrate the workspace with similar light conditions as when \acrshort{if} is building, if possible.
	\item Refer to equation \ref{eq:tagcalib}. For the tag calibration we need to measure $\hat{T}_{WR}$ manually. Because the accuracy this of procedure is high (we use the industrial arm for it), we can use $\hat{T}_{WR}$ as a health check. Thus, after calibration, run the base estimator and compare $\hat{T}_{WR}$ with $\tilde{T}_{WR}$. In our experiments we achieved a pose difference between those two poses of $\le$ 1.2 mm and $\le$ 0.06 degree.To do so, refer to section \ref{subsec:camcalib}: the calibration results can for example be improved by choosing the tag $T_j$ that gives the smallest absolute pose difference. An other possibility would be to verify that the monocular camera and tag calibration results are satisfying.
	\item When using the tag poses from \acrshort{rcars} directly (see \ref{sec:poses}), make sure the tags  fill approximately $\ge$5\% of the image plane. Adjust the tag size or camera lens accordingly.
\end{itemize}

\section {Robot pose estimation tests}
Using tag corners directly (\ref{sec:corn}) came up at the end of thesis which is why the following tests were done using the tag poses from \acrshort{rcars} (\ref{sec:poses}). In the upcoming tests, we are interested in the measurement errors \textit{at the end-effector} because after repositioning of the base we have to reattach accurate enough such that the mesh detection finds the correct wires. We estimate $\tilde{T}_{WR}$, whereas  $\hat{T}_{RE}$ is given by the industrial arm. Clearly, $\tilde{T}_{WE} = \tilde{T}_{WR} * \hat{T}_{RE}$. Thus, a measurement error of $x$ at the base has a different impact at the end-effector. $\tilde{T}_{WE}$ is compared with $\hat{T}_{WE} = \hat{T}_{WR} * \hat{T}_{RE}$ which is retrieved using \textit{one} measurement of the manual procedure mentioned in \ref{sec:tagcalib}.
\begin{table}[ht]
\begin{tabular}{|>{\centering\arraybackslash}m{6.6cm}|>{\centering\arraybackslash}m{6.6cm}|}
	\hline \textbf{Test case} & \textbf{Test result} \\
	\hline Take 10 consecutive base state estimations from the same robot pose. Compare $\hat{T}_{WE}$ with $\tilde{T}_{WE_{1..10}}$.  & Low variance. Absolute pose does not change more than 0.5mm/ 0.01 degree at the end-effector\\ 
	\hline Drive the arm to different locations in space. Compare $\hat{T}_{WE}$ with $\tilde{T}_{WE_{i}}$. The idea of this test is to see whether the base state changes when the arm affect the \acrshort{com} of the robot.  &  See figure \ref{fig:posediffest}. Maximum/mean absolute distance error is: 3.67/2.51mm, Maximum/mean angle error is: 0.17/0.15 degree. Different arm poses do affect the base pose.  \\ 	
	\hline Equip the end-effector with a measurement tip $t_1$. Reposition/drive the base to different location, estimate its pose and reattach to a known pose in space marked with a measurement tip $t_2$. Manually compare the distance difference of $t_1$ and $t_2$. &  Five test drives were done including translation ($\sim$ 15-50mm) and rotation (yaw angle $\sim$ 20  $^{\circ}$) of the robot base observing always 4-5 tags. Absolute measurement accuracy: $\sim$ 2-5 mm. No visable error in rotation.  \\ 	
	\hline 
\end{tabular} 
\caption[Robot pose estimation test cases and results]{Robot pose estimation test cases and results}
\label{tab:poseesttest}	
\end{table}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.65\linewidth]{images/baseest.pdf}
\caption{Pose difference of $\tilde{T}_{WE}$ and $\hat{T}_{WE}$ at different end-effector locations}
\label{fig:posediffest}
\end{figure}
\section {Mesh detection tests}
For the following mesh detection tests, we do not update the mesh computer model (expected wires) after a  measurement.
\begin{table}[!htb]
\begin{tabular}{|>{\centering\arraybackslash}m{6.6cm}|>{\centering\arraybackslash}m{6.6cm}|}
	\hline \textbf{Test case} & \textbf{Test result} \\
	\hline Move the arm to different mesh locations using $\tilde{T}_{WE}$. Compare absolute positional and angular difference of expected continuous wire with measured wire. For the positional difference, we compare the two middle points of the wires. &  See figure \ref{fig:wiredect}. All line pairs correctly matched. Errors and variance reasonably low. We observe a systematic positional error since we don't update the mesh computer model. \\ 
	\hline Consider figure \ref{fig:worstcase}. In this test we try to provoke such a worst case. We artificially change the computer model of the mesh and manually observe whether line pair matching still works &
	\begin{tabular}{|>{\centering\arraybackslash}m{4cm}|>{\centering\arraybackslash}m{1.6cm}|}
	 \hline \textbf{deviation} & \textbf{correct matches} \\ 
	 \hline x/y off by 0.8 cm & 37/37 \\
	 \hline x/y off by 1 cm & 34/37 \\
	 \hline x/y off by 1.5 cm & 14/37 \\
	 \hline y off by 2cm & 37/37 \\
	 \hline base yaw angle off by 0.2 degree & 37/37 \\
	 \hline base yaw angle off by 0.4 degree & 30/37 \\
	 \hline base yaw angle off by 0.2 degrees and x/y off by 1 cm & 31/37 \\
	 \hline 
	 \end{tabular}
	 \\ 		
	\hline 
\end{tabular} 
\caption[Mesh detection test cases and results]{Mesh detection test cases and results}
\label{tab:meshdetectiontest}	
\end{table}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.65\linewidth]{images/wiredect.pdf}
\caption{Pose difference of expected wire versus measured wire at different mesh locations}
\label{fig:wiredect}
\end{figure}


\section {System tests}
The goal of this final test is to prove, that the system works as a whole under laboratory conditions. Again, we do not update the computer model of the mesh after detecting a measurement. For every wire measurement we update the pose of the robot to do the correction step.
\begin{table}[!htb]
\begin{tabular}{|>{\centering\arraybackslash}m{5.6cm}|>{\centering\arraybackslash}m{7.6cm}|}
	\hline \textbf{Test case} & \textbf{Test result} \\
	\hline Reposition the base, estimate the robots state, reattach the tool, do the correction step and continue building &  Refer to figure \ref{fig:systest}. As expected, the critical point is right after repositioning of the base. For every mesh node, we detect the wire and do the end-effector correction step. After successfully applying the first correction step, later wire measurements are closer to the expected wire as to be depicted in the figure.  \\ 
	\hline Autonomously build 4 layers with 2*37 (for each side of the wall) mesh nodes. Do a correction step at ever new welding point. & 295/296 correct matches, no collisions during welding. Since we do not update the computer model of the mesh, it systematically deviates from the measurements the more we build. Explained in more details in chapter \ref{sec:futurework}. \\
	\hline 
\end{tabular} 
\caption[System test cases and results]{System test cases and results}
\label{tab:systest}	
\end{table}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{images/systest2.pdf}
\caption{Tool reattachment and mesh detection at every mesh node while building}
\label{fig:systest}
\end{figure}
