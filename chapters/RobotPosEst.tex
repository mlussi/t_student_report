\chapter{Robot pose estimation}
\label{ch:est}
For estimating the robots base pose, we propose a feature based localization approach based on cameras detecting APRIL\footnotemark[5] tags. Such a tag is a printable artificial landmark with a unique id. The advantage of using these tags is that a reliable open-source tag detection is already provided. We also conducted experiments using self-made features such as colored spheres that are detected as circles in an image. However, the detection (using hough-cricles\footnotemark[6]) was more time- consuming and less reliable and accurate than using tags. Efforts have been made to also analyze feature-less localization using 3d objects tracking \cite{lampart}. However, these methods are very hard to apply for the workspace of the \acrshort{if} since the mesh mold has a repetitive structure without texture. \\
The chapter is organized as follows: After defining some notation we describe the general optimization problem just using one camera observing several tags. This method is then applied to the real \acrshort{if} environment where both the calibration of the workspace and the actual robot pose estimation expressed in the world frame is shown.

\section{Notation}
\label{sec:notations}
For the upcoming sections in this chapter, we use the following notation:
\begin{table}[ht]

\begin{tabular}{|>{\centering\arraybackslash}m{1cm}|>{\centering\arraybackslash}m{12.1cm}|}
	\hline $A_{i} $ &  Capital letters are used to define a right-handed 3d \acrlong{cf} (\acrshort{cf}) with $x$,$y$,$z$ axis.  \\ 
	\hline $b_{j}$ &  Lower case letters are used to define a vector in 3d space with $x$,$y$,$z$ coordinates.  \\ 	
	\hline ${t}_{A_{i}b_{j}}$ &  Describes a vector that goes from the origin of the \acrshort{cf} $A_{i}$ to the point $b_{j}$ in 3d space. Or in other words: point $b_j$ expressed in frame $A_i$. It has 3 elements $x$,$y$,$z$ and is $ \in \mathbb{R}^3$. We also call it  \textit{position}.  \\ 
	\hline ${T}_{A_{i}B_{j}}$ & Describes the transformation from \acrshort{cf} $A_{i}$ to \acrshort{cf} $B_{j}$. It can be represented as a matrix that consists of a translation (position $x$,$y$,$z$) and a rotation ($roll$, $pitch$, $yaw$ represented as quaternion $w$,$x$,$y$,$z$) and is $ \in SO(3)$. We also call it  \textit{pose}.  \\ 
	\hline $\tilde{T}$,$\tilde{t}$ & Pose or position having a tilde on top of it are parameters that we trying to optimize using a cost function. we call them \textit{optimization parameters} \\ 	
	\hline $\hat{T}$,$\hat{t}$ & Pose or position having a hat on top of it are \textit{measurements}. \\ 		
	\hline 
\end{tabular} 
\caption[Notation used for the robot pose estimation]{Notation used for the robot pose estimation}
\label{tab:notations}	
\end{table}

\footnotetext[5]{\url{http://april.eecs.umich.edu/wiki/index.php/AprilTags}}
\footnotetext[6]{\url{http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_circle/hough_circle.html}}

\section{Batch estimation using APRIL tags}
Batch estimation was first introduced 1997 with the goal to match point clouds from different robot poses. The point clouds were generated using range measurement sensors such as lasers or ultrasonics \cite{batchest}. The idea of Batch Estimation is to take a whole 'Batch' of measurements and formulate one optimization problem that solves for the robot poses (or any other useful optimization parameters) at different measurement times . \\
Instead of range measurement sensors we use cameras that are detecting APRIL tags\footnotemark[5]. 

Using parts of the software \acrshort{rcars} \cite{rcars} together with APRIL tags we get the following measurements:
\begin{itemize}
	\item \textbf{Tag corners:} \acrshort{rcars} returns the $x$,$y$ position in the image of all four tag corners including the tag id.
	\item \textbf{Tag poses:} \acrshort{rcars} returns the 3d pose of the tag with respect to the camera frame including the tag id. In other words: returns $\hat{T}_{CT_{id}}$. It does that individually for each tag using the well known \acrshort{cv} function \textit{solvePnP()}\footnotemark[7]. The tag frame $T_{id}$ is defined in the middle of the tag with the $z$ axis being perpendicular to the tag plane.
\end{itemize}

\footnotetext[7]{\url{http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html}}

\subsection{Optimization problem using tag poses}
\label{sec:poses}
In this section we swipe around with one camera that measures the poses of an arbitrary amount of tags. Note, that we use the poses of \acrshort{rcars} directly:
\begin{figure}[ht]
\centering
\includegraphics[width=0.7\linewidth]{images/poses.pdf}
\caption{Swiping around with the camera measuring the pose of tags from different poses}
\label{fig:poses}
\end{figure}

$C_{0..m}$ are frame that represent the camera at different poses whereas $T_{0..n}$ represent the tag frame which are stationary lying somewhere in the world. We aim to solve for the following optimization parameters:
\begin{itemize}
	\item \textbf{$\tilde{T}_{WC_{0..n}}$}: The camera poses at different measurement times expressed in the world frame.
	\item \textbf{$\tilde{T}_{WT_{0..m}}$}: The tag poses expressed in the world frame.
\end{itemize}

\acrshort{rcars} provides us with $\hat{T}_{C_{0..m}T_{0..n}}$ so we use our optimization parameters as follows:
\begin{equation} 
\tilde{T}_{C_{i}T_{j}} = (\tilde{T}_{WC_{i}})^{-1} * \tilde{T}_{WT_{j}} 
\end{equation}
The optimization problem can therefore be expressed as follows:
 
\begin{equation} 
\label{eq:poses}
W = \underset{\tilde{T}_{C_{i}T_{j}}}{\operatorname{argmin}}\sum\limits_{i=0}^m\sum\limits_{j=0}^n
\norm{\hat{T}_{C_{i}T_{j}} - \tilde{T}_{C_{i}T_{j}}}_{\Sigma^{-1}}
\end{equation}

Note that we use the \textit{mahalanobis distance}\footnotemark[7] for each sum (called residuals) of the optimization problem. The difference to the euclidean distance is, that every element of the optimization parameters can be weighted individually. In the example above we optimize a pose which consist of rotation and translation expressed in different units. Using individual weights ensures that comparing distances with rotational angles is possible. Even if all units are the same we still can use different weights, since it would be possible that e.g. a distance measurement is more reliable in $x$ direction than in $z$ direction. The norm is indexed with $\Sigma^{-1}$ to indicate that the mahalanobis distance has a covariance $\Sigma^{-1}$. Note that if $\Sigma^{-1} = I$, the mahalanobis distance reduces to the euclidean distance. Each element is weighted with $\frac{1}{stddev_e}$ which is also called \textit{stiffness}.
\footnotetext[7]{\url{https://en.wikipedia.org/wiki/Mahalanobis_distance}}
\clearpage

\subsection{Optimization problem using tag corners}
\label{sec:corn}
In this section we also swipe around with a camera measuring tags from different poses. However, this time we just retrieve the pixel coordinates $x$,$y$ of all tag corners from \acrshort{rcars}: 

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{images/corners.pdf}
\caption{Swiping around with one camera measuring pixel coordinates of tag corners from different poses}
\label{fig:corners}
\end{figure} 
For the corners of each tag we define the vectors $e_{0..3}$ (\textit{e} stands for \textit{ecke} which mean corner in German). We define a right-handed \acrshort{cf} in the middle of the tag as follows: \\
\begin{figure}[ht]
\centering
\includegraphics[width=0.3\linewidth]{images/cf.pdf}
\caption{Definition of a coordinate frame lying the in middle of the tag}
\label{fig:tagcf}
\end{figure} 
\clearpage
Thus, the corner positions have the following values: 
\begin{equation} 
\label{eq:corncf}
t_{T_{j}e_{0}} = \begin{bmatrix} -s & s & 0 \end{bmatrix}^T, t_{T_{j}e_{1}} = \begin{bmatrix} s & s & 0 \end{bmatrix}^T, t_{T_{j}e_{2}} = \begin{bmatrix} -s & -s & 0 \end{bmatrix}^T, t_{T_{j}e_{3}} = \begin{bmatrix} s & -s & 0 \end{bmatrix}^T
\end{equation}
whereas $s=\frac{tagsize}{2}$. \\ Since we measure pixel coordinates, we need to define a function that projects a position in 3d to the image plane of the camera (2d). Assuming we have a rectified image this function is defined as.
\begin{equation}
\label{eq:proj}
p(t) = \begin{bmatrix} f_x(\frac{x}{z}) + c_x \\ f_y(\frac{y}{z}) + c_y \end{bmatrix} \in \mathbb{R}^2
\end{equation}
Remember that $t$ represents a position with $x$,$y$,$z$ coordinates, see \ref{sec:notations}. For detailed explanation of image plane projection $p()$ see\footnotemark[7]. \\
The optimization parameters are the same as in section \ref{sec:poses}. We define: 
\begin{equation}
\label{eq:optparam}
\tilde{t}_{C_{ij}e_{k}} = (\tilde{T}_{WC_{i}})^{-1} * \tilde{T}_{WT_{j}}  * t_{T_{j}e_{k}}
\end{equation}
$C_{ij}$ means camera at pose $i$ observing tag $j$. The result of the above equation is a position in 3d space. The optimization is done is 2d space by projecting the position to the image plane:

\begin{equation} 
\label{eq:corners}
W = \underset{\tilde{t}_{C_{ij}e_{k}}}{\operatorname{argmin}}\sum\limits_{i=0}^m\sum\limits_{j=0}^n\sum\limits_{k=0}^3
\norm{\hat{m}_{C_{ij}e_{k}} - p(\tilde{t}_{C_{ij}e_{k}})}_{\Sigma_{p}^{-1}}
\end{equation}
where $\hat{m}_{C_{ij}e_{k}}$ is the pixel measurement of the tag corners provided by \acrshort{rcars}.



\subsection{Comparison}
A reasonable way of comparing the two different approaches of formulating the optimization problem is to compare the re-projection error with each other. Calculating the re-projection error for each approach works as follows: take the estimations $\tilde{T}_{WC_{0..n}}$, $\tilde{T}_{WT_{0..m}}$ after optimization is done, define the position of the edges using equation \ref{eq:corncf}, re-project them using equation \ref{eq:proj} and finally compare it with the pixel measurements given by \acrshort{rcars}. This is called the \textit{re-projection error}. On a dataset with around 500 tag- (2000 corner-) measurements and 10 different tag ids we get:

\clearpage

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{/home/manuel/master_thesis/t_student_report/images/poses_vs_corners.pdf}
\caption{Re-projection error of poses versus corners}
\label{fig:compar}
\end{figure} 

We clearly observe that using the corners directly has a lower re-projections error. This makes sense due to multiple reasons:
\begin{itemize}
	\item \textbf{More measurements}: For every tag measurement we have 4 corners with $x$,$y$ coordinates resulting in 8 measurements. When we use the tag poses directly we only have 6 measurements (position $x$,$y$,$z$ and rotation $pitch$,$roll$,$yaw$).
	\item \textbf{Independent variance}: The variance of the pixel measurement is independent of how far away the tags are. However, when using the pose from \acrshort{rcars} directly the variance of the pose changes depending on its relative translation and rotation \acrshort{wrt} the camera frame \cite{rcars}.  
	\item \textbf{solvePnP()}: In our experiments, we observed  outliers in the rotational part of the  \acrshort{rcars} tag pose estimation which was approx. +/- 25 degrees. We assume, that this could come from solvePnP() which does not always find a global minima when estimating an individual tag pose.
\end{itemize}
\clearpage

\section{IF workspace calibration}
While building the mesh mold we have the following hardware setup: \\
\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{/home/manuel/master_thesis/t_student_report/images/overview.jpg}
\caption{Setup of the \acrshort{if} workspace with the relevant frames to calibrate.}
\label{fig:overview}
\end{figure} 
As to be seen in figure \ref{fig:overview} the setup consists of the following frames:
\begin{itemize}
	\item \textbf{\circled{1} $\rightarrow$  World \acrshort{cf} ($W$)}: The world frame can be put on an arbitrary place in the world.
	\item \textbf{\circled{3} and \circled{5} $\rightarrow$ Robot and End-effector \acrshort{cf} ($R$, $E$)}: The robot and the end-effector frame is given by the industrial arm.
	\item \textbf{\circled{4} $\rightarrow$ Base Camera Mount \acrshort{cf} ($M_i$)}: This is the camera frame at the mount positions of each camera. Currently, we use two cameras but the system will be designed such that the \acrshort{if} can be equipped with an arbitrary amount of base cameras. See \ref{subsec:camcalib} for detailed explanation.
	\item \textbf{\circled{2} $\rightarrow$ Tag \acrshort{cf} ($T_j$)}: The tag frames are defined as in figure \ref{fig:tagcf}.	
	\item \textbf{\circled{6} $\rightarrow$ Left and right mesh detection stereo camera \acrshort{cf} ($C_l$,$C_r$)}: Cameras used to detect the spatial mesh, see \ref{ch:det}. No abbreviation used for this frame because it is not used in this chapter.
\end{itemize}

The goal of this section is to define a similar optimization problem as in \ref{sec:corn} that solves for $\tilde{T}_{M_{0}R}$, $\tilde{T}_{WT_{j}}$ and $\tilde{T}_{M_{0}M_{i}}$. We call the estimation of these poses the workspace calibration. It has to be done only once before building the mesh mold. After doing this, we can estimate the robots pose ($\tilde{T}_{WR}$) (see \ref{sec:poseest}). 

\subsection{Robot base calibration}
\label{subsec:basecalib}
The goal of this sub section is to find $\tilde{T}_{M_{0}R}$. Since the industrial arm of the \acrshort{if} provides us with an accurate measurement of $\hat{T}_{RE}$ we can rigidly attach a tag somewhere at the end-effector and swipe around with it in front of a mounted camera. We will only describe the optimization parameters in the form of equation \ref{eq:optparam} as the rest is solved similar as in \ref{sec:corn}. We optimize for:
\begin{equation}
\overset{i}{\tilde{t}_{M_{0}e_k}} = \tilde{T}_{M_{0}R} * \overset{i}{\hat{T}_{RE}} * \tilde{T}_{ET} * t_{Te_{k}}
\end{equation}
$\overset{i}{\hat{T}_{RE}}$ is the pose measurement $i$ of the end-effector provided by the industrial arm. $M_0$ should be the camera that is better reachable with the arm.  $\tilde{T}_{ET}$ is also being optimized but beside using it as a health check of the solution we don't need it for further calculations. What we later need is $\tilde{T}_{M_{0}R}$.
\subsection{Tag calibration}
\label{sec:tagcalib}
The goal of this sub section is to find $\tilde{T}_{WT_{j}}$. We can calculate that for every Tag $j$ by using the following chain:
\begin{equation}
\label{eq:tagcalib}
\tilde{T}_{WT_{j}} = \hat{T}_{WR} * (\tilde{T}_{M_{0}R})^{-1} * \tilde{T}_{M_{0}T{j}}
\end{equation}
$\hat{T}_{WR}$ is measured with an accurate but extensive and manual procedure using a mechanical measurement tip as mentioned in \ref{sec:problem}. $\tilde{T}_{M_{0}R}$ is estimated as described in the sub section above. $\tilde{T}_{MT{j}}$ is estimated as described in \ref{sec:corn} with the difference that we set frame $M=$ frame $W$. In other words: We take at least one tag measurement at the position where we mount the camera ($M$) and for the optimization we set $T_{WM} = I$ as a constant. Clearly, we take many other tag measurements from other camera poses that are not at the mount position to make full use of the advantages of batch estimation.

\subsection{Camera calibration}
\label{subsec:camcalib}
Since we already calculated the pose of $M_{0}$ \acrshort{wrt} to the world in the above equations we finally need to find the transformation from $M_{0}$ to the other camera mount frames to close the chain. For that we simply calculate:
\begin{equation}
\tilde{T}_{M_{0}M_{i}} = \tilde{T}_{M_{0}T_{j}} * (\tilde{T}_{M_{i}T_{j}} )^{-1} 
\end{equation}
Again, we estimate $\tilde{T}_{M_{i}T_{j}}$ as described in \ref{sec:corn} with some minor differences mentioned in the subsection above.
\section{IF global pose estimation}
\label{sec:poseest}
After calibration of the workspace we can solve for the pose of the robot in the world frame ($\tilde{T}_{WR}$). Again we use the approach presented in \ref{sec:corn}  and formulate our estimate as:
\begin{equation}
\tilde{t}_{M_{i}e_{k}} = (\tilde{T}_{M_{0}M_{i}})^{-1} * \tilde{T}_{M_{0}R} *  (\tilde{T}_{WR})^{-1} * \tilde{T}_{WT_{j}} * t_{T_{j}e_{k}}
\end{equation}
All poses except $\tilde{T}_{WR}$ are being estimated during calibration procedure and can be seen as constants. Thus, the solver only has to solve for one pose. A solution is possible as long as one camera sees at least one tag. The more tags we measure the better the estimation is. This is why in figure \ref{fig:overview} we use two cameras to make sure we always get enough tag measurement. To improve the systems accuracy and robustness, it can easily be enhanced with more cameras or more tags. 